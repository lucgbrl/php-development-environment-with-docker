## Template de ambiente de desenvolvimento


Este repositório possui alguns arquivos de configuração para executar PHP e Nginx por meio do docker-compose.
Estou utilizando as imagens do NginX como servidor e do PHP-FPM por serem mais adequadas ao serviço em que é necessário uma maior quantidade de requisições e um menor consumo de memória.

## Tabela de conteúdos

- Introdução
- Instalação
- Como executar?
- Como colaborar com este documento?
- Colaboradores


## Introdução

Sessão ainda pendente!

## Instalação

O processo de instalação do Docker e docker-compose pode ser feito seguindo a documentação oficial.

Para fazer uso do repositório o código PHP deverá ser adicionado dentro da pasta SRC. Demais modificações na estrutura de pastas devem ser refletidos no docker-compose.yml e também no arquivo de configuração do nginx que é carregado quando o volume é montado pelo Docker. 



## Como executar?


Para executar os serviços é necessário fazer uso do comando a seguir:

```
$ docker-compose up -d
```

A flag -d fará com que os containers sejam executados em segundo plano, liberando o terminal em seguida.


Para interromper a execução do serviço pode ser utilizado o comando a seguir:

```
$ docker-compose down
```


### Como contribuir com este documento?

Para colaborar com este documento basta realizar a abertura de uma issue ou de um merge request ou enviar uma mensagem via DM no Twitter para @lucgbrl que irei responder o mais rápido o possível.


## Colaboradores 


Lucas Gabriel G. dos Santos, Engenheiro de Computação (UFC), MBA Engenharia de Software (FAMEESP), Bolsista BIT em FUNCAP/CE. 

## Licensa

Licensa MIT. 2021.


